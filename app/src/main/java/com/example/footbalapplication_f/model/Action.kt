package com.example.footbalapplication_f.model

data class Action(
        val player: Player? =null,
        val player1 : Player? = null,
        val player2 : Player? = null,
        val goalType: Int? = null
)