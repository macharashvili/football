package com.example.footbalapplication_f.model

import android.drm.DrmStore

data class TeamAction(
        val actionType: Int? = null,
        val teamType: Int? = null,
        val action: Action? = null
)