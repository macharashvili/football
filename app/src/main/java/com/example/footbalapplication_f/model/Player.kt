package com.example.footbalapplication_f.model

data class Player(
        val playerName: String? = null,
        val playerImage: String? = null
)