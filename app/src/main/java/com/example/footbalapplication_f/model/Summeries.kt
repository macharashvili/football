package com.example.footbalapplication_f.model

data class Summaries(
        val actionTime: Int? = null,
        val team1Action: List<TeamAction>? = null,
        val team2Action: List<TeamAction>? = null
)