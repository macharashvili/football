package com.example.footbalapplication_f.response

import com.example.footbalapplication.model.Match

data class MatchResponse(val resultCode: Int? = 0,
                         val match: Match? = null)