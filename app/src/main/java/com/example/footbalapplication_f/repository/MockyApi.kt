package com.example.footbalapplication

import com.example.footbalapplication.response.MatchResponse
import retrofit2.Call
import retrofit2.http.GET

interface MockyApi {
    @GET("5b9264193300006b00205fb9")
    fun getAllItems() : Call<MatchResponse>
}