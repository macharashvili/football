package com.example.footbalapplication_f.enums

enum class GoalType(val value: Int) {
    GOAL(1), OWN_GOAL(2)
}