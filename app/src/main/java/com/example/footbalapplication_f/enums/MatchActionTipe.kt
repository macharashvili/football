package com.example.footbalapplication_f.enums

enum class MatchActionTipe(val value: Int) {
    GOAL(1), YELLOW_CARD(2), RED_CARD(3), SUBSTITUTION(4)
}