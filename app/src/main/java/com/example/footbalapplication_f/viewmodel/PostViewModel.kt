package com.example.footbalapplication_f.viewmodel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide.init
import com.example.footbalapplication.repository.MatchRepository
import com.example.footbalapplication.response.MatchResponse
import com.squareup.picasso.Picasso

class PostViewModel: ViewModel() {
    private val matchRepository: MatchRepository = MatchRepository().getInstance()
    private var matchRepons: MutableLiveData<MatchResponse> = matchRepository.getMatches()

    val goals: MutableLiveData<String> = MutableLiveData()
    val footbaler_name: MutableLiveData<String> = MutableLiveData()
    val football_player: MutableLiveData<String> = MutableLiveData()
    val point: MutableLiveData<String> = MutableLiveData()

    init {
        matchRepons = matchRepository.getMatches()
        matchRepons.observeForever {
            goals.value = ("${it.match!!.matchDate} ${it.match.matchSummary}")
            footbaler_name.value = ("${it.match.matchDate} ${it.match.matchSummary}")
            football_player.value = ("${it.match.matchDate} ${it.match.matchSummary}")
            point.value = ("${it.match.matchDate} ${it.match.matchSummary}")
        }
    }

    fun getApiResponse(): LiveData<MatchResponse> {
        return matchRepons
    }

    @BindingAdapter("src")
    fun loadImage(view: ImageView, src: String) {
        Picasso.get().load(src).into(view)
    }
}