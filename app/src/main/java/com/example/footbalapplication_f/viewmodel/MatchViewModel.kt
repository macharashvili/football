package com.example.footbalapplication_f.viewmodel

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import com.example.footbalapplication.R
import com.example.footbalapplication.databinding.ActivityMainBinding
import com.example.footbalapplication.repository.MatchRepository
import com.example.footbalapplication.response.MatchResponse
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.view.*


class MatchViewModel: ViewModel() {
    private val matchRepository: MatchRepository = MatchRepository().getInstance()
    private val matchResponse: MutableLiveData<MatchResponse> = matchRepository.getMatches()

    val text: MutableLiveData<String> = MutableLiveData()

    init {
//        matchResponse = matchRepository.getMatches()
        matchResponse.observeForever {
            text.value = "${it.match!!.matchDate} ${it.match.stadiumAdress}"
        }
        }

    fun getApiResponse(): LiveData<MatchResponse> {
        return matchResponse
    }
}