package com.example.footbalapplication_f.viewmodel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide.init
import com.example.footbalapplication.repository.MatchRepository
import com.example.footbalapplication.response.MatchResponse
import com.squareup.picasso.Picasso

class PlayerViewModel:  ViewModel() {
    private val matchRepository: MatchRepository = MatchRepository().getInstance()
    private var matchRepons: MutableLiveData<MatchResponse> = matchRepository.getMatches()

    val substitution: MutableLiveData<String> = MutableLiveData()
    val sub_player_one_name: MutableLiveData<String> = MutableLiveData()
    val sub_player_two_name: MutableLiveData<String> = MutableLiveData()
    val sub_player_one_ic: MutableLiveData<String> = MutableLiveData()
    val sub_player_two_ic: MutableLiveData<String> = MutableLiveData()

    init {
        matchRepons = matchRepository.getMatches()
        matchRepons.observeForever {
            substitution.value = "${it.match!!.matchDate} ${it.match.matchSummary}"
            sub_player_one_name.value = "${it.match.matchDate} ${it.match.matchSummary}"
            sub_player_two_name.value = "${it.match.matchDate} ${it.match.matchSummary}"
            sub_player_one_ic.value = "${it.match.matchDate} ${it.match.team1}"
            sub_player_two_ic.value = "${it.match.matchDate} ${it.match.team2}"
        }
    }

    fun getApiResponse(): LiveData<MatchResponse> {
        return matchRepons
    }

    @BindingAdapter("src")
    fun loadImage(view: ImageView, src: String) {
        Picasso.get().load(src).into(view)
    }
}