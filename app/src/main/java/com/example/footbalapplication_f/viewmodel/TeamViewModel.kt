package com.example.footbalapplication_f.viewmodel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide.init
import com.example.footbalapplication.repository.MatchRepository
import com.example.footbalapplication.response.MatchResponse
import com.squareup.picasso.Picasso

class TeamViewModel:  ViewModel() {
    private val matchRepository: MatchRepository = MatchRepository().getInstance()
    private var matchResponse: MutableLiveData<MatchResponse> = matchRepository.getMatches()

    val image_id: MutableLiveData<String> = MutableLiveData()
    val teamNameLabel: MutableLiveData<String> = MutableLiveData()

    init {
//        matchResponse = matchRepository.getMatches()
        matchResponse.observeForever {
            image_id.value = "${it.match!!.matchDate} ${it.match.team1}"
            teamNameLabel.value = "${it.match.matchDate} ${it.match.team2}"
        }
    }

    fun getApiResponse(): LiveData<MatchResponse> {
        return matchResponse
    }

    @BindingAdapter("src")
    fun loadImage(view: ImageView, src: String) {
        Picasso.get().load(src).into(view)
    }
}