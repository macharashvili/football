package com.example.footbalapplication_f.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.example.footbalapplication.R
import kotlinx.android.synthetic.main.post_view.view.*
import kotlinx.android.synthetic.main.team_view.view.*

class PostView @JvmOverloads  constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {
//    var playerImage: ImageView? = null
//        set(value) {
//            field = value
//            if (value != null)
//                messi?. setImageResource()
//        }

    var playerName: String? = null
        set (value) {
            field = value
            if (value != null)
                footbaler_name?. text =value
        }

    var playergoals: String? = null
        set (value) {
            field = value
            if (value != null)
                footbaler_name?. text =value
        }

//    var playercard: ImageView? = null
//        set (value) {
//            field = value
//            if (value != null)
//                card.setImageResource()
//        }

    init {
        View.inflate(context, R.layout.team_view, this)

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.TeamView, 0, 0)
            playerName = typedArray.getString(R.styleable.TeamView_name)
            playergoals= typedArray.getString(R.styleable.PostView_pv_goal)
//            playercard= typedArray.getString(R.styleable.PostView_pv_card)
        }
    }
}