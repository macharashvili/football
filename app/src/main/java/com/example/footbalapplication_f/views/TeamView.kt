package com.example.footbalapplication_f.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.example.footbalapplication.R
import kotlinx.android.synthetic.main.team_view.view.*

class TeamView @JvmOverloads  constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {
    var teamName: String? = null
    set(value) {
        field = value
        if (value != null)
            teamNameLabel?.text = value
    }

    init {
        View.inflate(context, R.layout.team_view, this)

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.TeamView, 0, 0)
            teamName = typedArray.getString(R.styleable.TeamView_name)
        }
    }
}