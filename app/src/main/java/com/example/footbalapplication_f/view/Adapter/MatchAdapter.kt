package com.example.footbalapplication_f.view.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.footbalapplication.R
import com.example.footbalapplication.enums.GoalType
import com.example.footbalapplication.enums.MatchActionTipe
import com.example.footbalapplication.model.Summaries
import com.example.footbalapplication.response.MatchResponse
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.player_view.view.*
import kotlinx.android.synthetic.main.point_layout.view.*
import kotlinx.android.synthetic.main.post_view.view.*
import kotlin.collections.arrayListOf as arrayListOf1

class MatchAdapter(private val item: MatchResponse, private val context: Context) :
        RecyclerView.Adapter<MatchAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.point_layout, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return item.match?.matchSummary!!.summaris!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val summary: Summaries? = item.match!!.matchSummary?.summaris?.get(position)

        holder.itemView.left_side.removeAllViews()
        holder.itemView.right_side.removeAllViews()

        for (team1Action in summary?.team1Action ?: arrayListOf1()) {
            var view = View.inflate(context, R.layout.post_view, null)

            when (team1Action.actionType) {
                MatchActionTipe.GOAL.value -> {
                    view.footbaler_name.text = team1Action.action?.player?.playerName
                    Picasso.get().load(team1Action.action?.player?.playerImage).into(view.football_player)
                    when (team1Action.action?.goalType) {
                        GoalType.GOAL.value -> view.ball_image.setImageResource(R.drawable.ball)
                        GoalType.OWN_GOAL.value -> view.ball_image.setImageResource(R.drawable.ball_read)
                    }
                }

                MatchActionTipe.YELLOW_CARD.value -> {
                    view.footbaler_name.text = team1Action.action?.player?.playerName
                    Picasso.get().load(team1Action.action?.player?.playerImage).into(view.football_player)
                    view.ball_image.setImageResource(R.drawable.yellow_card)
                }

                MatchActionTipe.RED_CARD.value -> {
                    view.footbaler_name.text = team1Action.action?.player?.playerName
                    Picasso.get().load(team1Action.action?.player?.playerImage).into(view.football_player)
                    view.ball_image.setImageResource(R.drawable.red_card)
                }
                MatchActionTipe.SUBSTITUTION.value -> {
                    view = View.inflate(context, R.layout.player_view, null)
                    view.sub_player_one_name.text = team1Action.action?.player1?.playerName
                    view.sub_player_two_name.text = team1Action.action?.player2?.playerName
                    Picasso.get().load(team1Action.action?.player1?.playerImage).into(view.sub_player_one_ic)
                    Picasso.get().load(team1Action.action?.player2?.playerImage).into(view.sub_player_two_ic)
                }
            }

            holder.itemView.left_side.addView(view)
        }

        for (team2Action in summary?.team2Action ?: arrayListOf1()) {
            var view = View.inflate(context, R.layout.post_view, null)

            view.scaleX = -1f
            view.player_name.scaleX = -1f
            when (team2Action.actionType) {
                MatchActionTipe.GOAL.value -> {
                    view.footbaler_name.text = team2Action.action?.player?.playerName
                    Picasso.get().load(team2Action.action?.player?.playerImage).into(view.football_player)
                    when (team2Action.action!!.goalType) {
                        GoalType.GOAL.value -> view.ball_image.setImageResource(R.drawable.ball)
                        GoalType.OWN_GOAL.value -> view.ball_image.setImageResource(R.drawable.ball_read)
                    }
                }

                MatchActionTipe.YELLOW_CARD.value -> {
                    view.footbaler_name.text = team2Action.action?.player?.playerName
                    Picasso.get().load(team2Action.action?.player?.playerImage).into(view.football_player)
                    view.ball_image.setImageResource(R.drawable.yellow_card)
                }

                MatchActionTipe.RED_CARD.value -> {
                    view.footbaler_name.text = team2Action.action?.player?.playerName
                    Picasso.get().load(team2Action.action?.player?.playerImage).into(view.football_player)
                    view.ball_image.setImageResource(R.drawable.red_card)
                }
                MatchActionTipe.SUBSTITUTION.value -> {
                    view = View.inflate(context, R.layout.player_view, null)
                    view.scaleX = -1f
                    view.player_name.scaleX = -1f
                    view.sub_player_one_name.text = team2Action.action?.player1?.playerName
                    view.sub_player_two_name.text = team2Action.action?.player2?.playerName
                    Picasso.get().load(team2Action.action?.player1?.playerImage).into(view.sub_player_one_ic)
                    Picasso.get().load(team2Action.action?.player2?.playerImage).into(view.sub_player_two_ic)
                }
            }
            holder.itemView.right_side.addView(view)
        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}