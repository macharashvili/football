package com.example.footbalapplication_f.view.Ui

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.RelativeLayout
import com.example.footbalapplication.model.Match
import com.example.footbalapplication.model.Team
import com.example.footbalapplication.response.MatchResponse
import com.example.footbalapplication.views.TeamView
import kotlinx.android.synthetic.main.match_preview_activity.*
import kotlinx.android.synthetic.main.match_teams_tile_view.*
import kotlinx.android.synthetic.main.text_view.*

internal class MatchPreviewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUi(match = createMatch())
    }

    private fun createMatch(): Match{
        var match = Match(
                team1 = Team(teamName = "team1", score = 1),
                team2 = Team(teamName = "team2", score = 2),
                matchTime = 23.34)

        return match
    }

    private fun initUi(match: Match? = null){
        teamOne?.teamName = match?.team1?.teamName
        teamTwo?.teamName = match?.team2?.teamName
        matchScore?.text = "${match?.team1?.score}:${match?.team2?.score}"
        matchTime?.text = "${match?.matchTime}"

//        var teamView = TeamView(context = this)
//        teamView.teamName = "bla bla"
//        matchTestContainer?.addView(teamView)
    }

    fun setSpannable(myText: String) : SpannableString{
        val spannableContent = setSpannable(myText)
        spannableContent.setSpan(ForegroundColorSpan(Color.GREEN), 0, myText.length/2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        return spannableContent
    }
}
