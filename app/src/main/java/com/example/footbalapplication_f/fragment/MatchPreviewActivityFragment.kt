package com.example.footbalapplication_f.fragment

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
//import com.example.footbalapplication.R
import com.example.footbalapplication_f.R
import com.example.footbalapplication_f.fragmentViewModel.MatchPreviewActivityViewModel

class MatchPreviewActivityFragment : Fragment() {

    companion object {
        fun newInstance() = MatchPreviewActivityFragment()
    }

    private lateinit var viewModel: MatchPreviewActivityViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.match_preview_activity_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MatchPreviewActivityViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
